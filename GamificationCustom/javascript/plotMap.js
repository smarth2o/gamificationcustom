var mapdata;
var user = $('#user_oid').val();
var curr_url = "http://esb.smarth2o.ro:9081/neighbourhood/ServiceViewSmartH2O/NeighbourhoodData/getNeighbourhood?user_id="+user;

$.ajax({ 
 	type: 'get',
    	//url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetNeighborhood/getCoordinates.do?user_oid="+user, 
		url: "http://131.175.141.236:8080/community/RestCall.jsp?curr_url="+curr_url, 
		dataType: 'json',
    	success: function(xhr,status){   
			mapdata = xhr;
			onSuccessMap();
    	},
    	error: function(xhr,status){

    	}
   });
   
   function onSuccessMap() { 
   
   function plotMap(id){
	  var geocoder;
	  var map;
	  var clickable = false;
	  if(id==('map_canvas_all')){
		clickable = true;
	  }	  
	  geocoder = new google.maps.Geocoder();
	  var latlng = new google.maps.LatLng(45.79367,9.198290000000043);
	  var mapOptions = {
		center: latlng,
		disableDefaultUI: true,
	  }
	  map = new google.maps.Map(document.getElementById(id), mapOptions);
	  map.setCenter(latlng);
	  var bounds = new google.maps.LatLngBounds ();

      var infowindow = new google.maps.InfoWindow();
      var marker, i;
	  
	  for(i=0;i<mapdata.length;i++){
	    
		(function(){
		var a = mapdata[i].address + ' ' + mapdata[i].city + ' ' + mapdata[i].country;
		var t = mapdata[i].lastname;
		var id = mapdata[i].oid;

		geocoder.geocode( { 'address': a}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
		  bounds.extend(results[0].geometry.location);
			  var marker;
			  if(id==user){
				  marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  icon:'GamificationCustom/images/home40.png',
					  clickable: clickable
				  });
			  }
			  else{
				  marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  icon:'GamificationCustom/images/home24.png',
					  clickable: clickable
				  });
			  }
			  var infoWindow = new google.maps.InfoWindow();
			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
				  infowindow.setContent(t);
				  infowindow.open(map, marker);
				}
			  })(marker, i));

		  map.fitBounds (bounds);

		} else {
		  
		}
		});
		}());
	  }

	}
	plotMap('map-canvas');

	$(".fMap").colorbox({
		html:'<div id="map_canvas_all" style="width:100%; height:100%;"></div>',
		scrolling:false,
		width:"80%",
		height:"80%",
		close: "Close",
		onComplete:function(){ plotMap('map_canvas_all'); }
	  });
   };
   











