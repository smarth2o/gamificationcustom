
var badgesdata;
var user_oid = $('#user_oid').val();
var score;
var areaTitles = [];
var areaIds = [];
var areaImages = ['GamificationCustom/images/social.png','GamificationCustom/images/info.png','GamificationCustom/images/drop_area.png','GamificationCustom/images/book.png'];
var cardinalities = [];
var max;
var h_new;
var i_new;

	
	$.ajax({ 
		type: 'get',
			//url: "http://131.175.141.236:8080/community/UserActivityCreditWebServiceREST/GetUserBadges/getBadges.do?user_id="+user_oid, 
			url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetUserBadges/getBadges.do?user_id="+user_oid,
			dataType: 'json',
			success: function(xhr,status){   
				badgesdata = xhr.badges_list;
				score = xhr.data;
				onSuccessBadge();
			},
			error: function(xhr,status){

			}
	   });
	   

function onSuccessBadge() { 
		for(var h=0;h<badgesdata.length;h++){
			areaIds.push(h);
			areaTitles.push(badgesdata[h].area);			
			cardinalities.push(badgesdata[h].badges.length);
			if(h==badgesdata.length-1){
				max = Math.max.apply(null, cardinalities);
			}
		}
         $(function() {
			badge_chart = new Highcharts.Chart(
				{
					chart: {
						renderTo: 'badgeChartCont'
					},
					title: {
						text: 'Your Badges',
						style: {
									fontSize: '14px',
									fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif'
								}
					},
					exporting: {
						enabled: false
					},
					xAxis: {
						//categories: ['Water Saving', 'Learning', 'Profiling', 'Participation'],
						categories: areaIds,
						labels: {
							x: -5,
							useHTML: true,
							formatter: function () {

								return '<img height="32" width="32" src="'+areaImages[this.value]+'" title="'+areaTitles[this.value]+'"></img>'
							}
							
						},
						gridLineWidth: 0
					},
					yAxis: {
						min: 0,
						max: 110,
						title: {
							text: ''
						},
						labels:
						{
						  enabled: false
						},
						gridLineWidth: 0
					},
					legend: {
						enabled: false
					},
					tooltip: {
						formatter: function() {
							if(this.series.name=='Current credits'){
								return '<b>'+ this.series.name +'</b>: '+ score[this.x].score;
							}
							if(this.series.name=='Badge'){
								return '<b>'+ this.series.name +'</b>: '+ Math.floor(this.y*0.01*badgesdata[this.x].max) + ' credits';
							}
						}
					},

					series: [{
						name: 'Remaining credits',
						type: 'bar',
						data: [{y: 100-Math.floor(score[0].score*100/badgesdata[0].max), color: '#aaa'}, {y: 100-Math.floor(score[1].score*100/badgesdata[1].max), color: '#aaa'}, {y: 100-Math.floor(score[2].score*100/badgesdata[2].max), color: '#aaa'},{y: 100-Math.floor(score[3].score*100/badgesdata[3].max), color: '#aaa'}],
						stacking: 'normal'
					}, {
						name: 'Current credits',
						type: 'bar',
						data: [{y: Math.floor(score[0].score*100/badgesdata[0].max), color: '#C2216E'}, {y: Math.floor(score[1].score*100/badgesdata[1].max), color: '#D28C22'}, {y: Math.floor(score[2].score*100/badgesdata[2].max), color: '#26A5DD'},{y: Math.floor(score[3].score*100/badgesdata[3].max), color: '#74AB44'}],
						stacking: 'normal'
					}
					 ]
					
					
				},function opt(badge_chart){

					
					var tempBadge = {};
					var tempMarker = {};
					var data;
					//for(var h=0; h < max; h++){
					for (var h=max-1; h >=0; h--) {
						
						data = [];
						for(var i=0; i < badgesdata.length; i++){

							tempBadge = {};
							
							if(h<badgesdata[i].badges.length){

								tempBadge.y = badgesdata[i].badges[h].score*100/badgesdata[i].max;
								
								tempMarker = {};
								tempMarker.symbol = 'url(data:image/png;base64,'+badgesdata[i].badges[h].icon.replace(/(\r\n|\n|\r)/gm,"")+')';
								tempMarker.height = '32';
								tempMarker.width = '32';
								tempBadge.marker = tempMarker;
								
							}
							else{
								tempBadge = null;
							}
							data.push(tempBadge);
							
							
						}

						badge_chart.addSeries({
								name: 'Badge',
								data: data,
								lineWidth : 0,
								enableMouseTracking: true,
								states : {
									hover : false
								}


						});
						$.each( $('#badgeChartCont').find('.highcharts-markers').children(), function(){
							var marker = $(this);
							// rotate 90 degrees around the middle point of the marker
							var rotateAttr = 'rotate(90,'+(parseFloat(marker.attr('x'))+parseFloat(marker.attr('width'))/2)+','+marker.attr('y')+')';
							marker.attr('transform',rotateAttr);
						});

					}

			});
    });
		
}	

/*
function getImage(badge_id,i,h){
	var im;
	return $.ajax({ 
		type: 'get',
			url: "http://127.0.0.1:8080/community/UserActivityCreditWebServiceREST/GetBadge/getBadge.do?id="+badge_id, 
			dataType: 'json',
			success: function(xhr,status){   
				badgesdata[i].badges[h].icon = xhr.image;
				if((h==0)&&(i==badgesdata.length-1)){
					onSuccessBadge();
				}
			},
	   });

}
*/





