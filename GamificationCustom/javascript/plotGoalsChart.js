var goaldata;
var goaldata_copy;
var images = [];
var curr_avg;
var user_oid = $('#user_oid').val();
var credits = $('#credits').val();

$.ajax({ 
 	type: 'get',
    	//url: "http://localhost:8080/community/goal.jsp?userID="+user_oid, 
		//url: "http://131.175.141.236:8080/community/UserActivityCreditWebServiceREST/GetUserConsumptionGoals/getGoals.do?user_id="+user_oid, 
		url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetUserGoals/getGoals.do?user_id="+user_oid, 
		dataType: 'json',
    	success: function(xhr,status){   
			//goaldata = xhr.data;
			goaldata = xhr;
			goaldata_copy = xhr;
			//secondCall();
			onSuccessGoal();
    	},
    	error: function(xhr,status){

    	}
   });

function secondCall(){
	var count = 0;
	var rec = function(){

		//var first = goaldata_copy.shift();
		$.ajax({ 
		type: 'get',
			url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetGoal/getGoal.do?id="+goaldata[count].id, 
			dataType: 'json',
			success: function(xhr2,status2){
				
				goaldata[count].image = xhr2.image;
				images.push(xhr2.image);
				if (count == goaldata.length - 1) 
				{
					onSuccessGoal();
					return;
				}
				count++;
				setTimeout(rec , 0);

			},
			error: function(xhr2,status2){

			}
	   });
		
	}
	setTimeout(rec, 0);

	

	
} 
 


function onSuccessGoal() { 

		
		

		
	curr_avg = 200;
	var scaled_avg;
	var goals = [];
	var scaled_goals = [];
	var sg = [];
	var ticks = [];
	var scaled_ticks = [];
	var titles = [];


	
	for(var h=0;h<goaldata.length;h++){
		var tempGoal = {};
		tempGoal.goal = goaldata[h].score;
		tempGoal.title = goaldata[h].title;
		tempGoal.image = goaldata[h].image;
		//tempGoal.image = images[h];
		goals.push(tempGoal);
		ticks.push(goaldata[h].score);
	}
	
	goals.sort(function(a,b) { return parseFloat(a.goal) - parseFloat(b.goal) } );
	ticks.sort();
	var max_of_array = goals[goals.length-1].goal;
	var h;
	
	for(h=0;h<goals.length;h++){
		(function(){
			
			var sc_go = Math.floor(goals[h].goal*100/max_of_array);
			var tempGoalSc = {};
			tempGoalSc.goal = sc_go;
			tempGoalSc.title = goals[h].title;
			tempGoalSc.image = goals[h].image;
			scaled_goals.push(tempGoalSc);
			sg.push(sc_go);
			//scaled_goals.push(sc_go);
		}());
	}
	//min tick
	scaled_ticks.push(0);
	for(h=0;h<ticks.length;h++){
		(function(){
			var sc_ti = Math.floor(ticks[h]*100/max_of_array);
			scaled_ticks.push(sc_ti);
		}());
		
	}
	//max tick
	scaled_ticks.push(118);

	scaled_avg = Math.floor(curr_avg*100/max_of_array);

	var colors = ['GreenYellow','Orange','OrangeRed','Crimson','Gold','Magenta','DarkGreen',];
	var plotLines = [];
	var found = false;
	var ind;
	
	for(var k=0;k<scaled_goals.length;k++){
		
		var temp = {};
		//var color = getRandomColor();
		var color = colors[k];
		temp.value = scaled_goals[k].goal;
		temp.color = color;
		temp.dashStyle = 'shortdash';
		temp.width = 4;
		temp.zIndex = 4;
		var label = {};
		//label.text = scaled_goals[k].title;
		label.text = '';
		label.style = {};
		label.style.color = color;
		label.align = 'center';
		temp.label = label;
	
		plotLines.push(temp);
		
		//check last badge active
		if((scaled_goals[k].goal>scaled_avg)&&(found==false)){
			found = true;
			ind = k;
		}
	}
	if(found){
		$('#goal_title').html('<b>You are: </b>' + scaled_goals[ind].title);
	}
	else{
		$('#goal_title').html('<b>Reduce your water consumption</b>');
	}
	
	

	
		$(function() {
			goalChart = new Highcharts.Chart(
			{
				chart: {
					type: 'column',
					renderTo: 'goalChartContainer',
					

				},
				title: {
					text: '',
				},
				xAxis: {
					categories: ['Next Goal'],
					labels:
					{
					  enabled: false
					}
				},
				legend: {
					enabled: false
				},
				yAxis: {
					title: null,
					min: 0,
					//max: 110,
					
					labels: {
							align: 'left',
							x: -10,
							useHTML: true,  
							formatter: function(){
								
								if($.inArray(this.value, sg)!=-1){
								//if($.grep(scaled_goals, function(e){ return e.goal == this.value; }).length>0)
									var index = $.inArray(this.value, sg);
									var tit = scaled_goals[index].title;
									if(scaled_goals[index].goal > scaled_avg){
										
										return '<img src="data:image/png;base64, '+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
										//return '<img src="'+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
									}		
									else{
										
										return '<img src="data:image/png;base64, '+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
										//return '<img src="'+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
									}
								}								
							}
						},
						
					//tickInterval: 1,
					//gridLineColor: 'transparent',
					plotLines: plotLines,
					tickPositions: scaled_ticks,

				},
				
				exporting: {
					enabled: false
				},
				series: [{
					name: 'Collected credits',
					data: [scaled_avg],
					marker: {
						enabled: false
					},
					pointWidth: 28,
					color: '#23AAE2',
					zIndex: 1
				}]
					
			},function(goalChart){
				goalChart.renderer.image("GamificationCustom/images/tube.png", 0, 0, goalChart.chartWidth, goalChart.chartHeight).attr({
				  zIndex: 3
				}).add();
				});
				goalChart.series[0].update({ data: [scaled_avg], marker: {enabled: false}, pointWidth: goalChart.chartWidth});


		});

}
