var jsondata;
var charts;
var avg = 0.5;
var neigh_average = 0.1;
var user = $('#user_oid').val();
var curr_url = "http://esb.smarth2o.ro:9081/consumption/ServiceViewSmartH2O/ConsumptionData/getConsumption?user_id="+user;

$.ajax({ 
 	type: 'get',
    	//url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetConsumptionData/getConsumpData.do?user_oid="+user, 
		//url: "http://89.121.250.90:8083/SmartH2O/ServiceViewSmartH2O/GetConsumptionData/getConsumption.do?user_id=1",
		url: "http://131.175.141.236:8080/community/RestCall.jsp?curr_url="+curr_url,
		dataType: 'json',
    	success: function(xhr,status){   
			jsondata = xhr;
			onSuccess();
    	},
    	error: function(xhr,status){

    	}
   });

   	function onSuccess(){
	
	var json = jsondata.reverse();
	//var json = jsondata.json;
	var dataSerie0 = [];
	var init_sum = 0;
	var init_count = 0;
	
	for(var i=0;i<json.length;i++){
	//for(var i=json.length-1;i>=0;i--){
		var obj = json[i];
		for(var key in obj){
			if(key=='timestamp'){
			   t = obj[key];
			   t = t.substring(0, 10);
			}
			else{
			   q = obj[key];
			   init_sum = init_sum + q;
			   init_count = init_count + 1;
			}
		}
		dataSerie0.push([Date.parse(t),q]);
	}

	avg = init_sum/init_count;

	var categoryImgs = ["GamificationCustom/images/clock.png","GamificationCustom/images/man.png","GamificationCustom/images/woman.png","GamificationCustom/images/son.png"];



         $(function() {
			charts = new Highcharts.StockChart(
				{
					chart: {
						type: 'line',
						renderTo: 'container',
						zoomType: 'x'
					},
					title: {
						text: ''
					},
					rangeSelector: {
						enabled : false
					},
					xAxis: {
						events: {
							setExtremes: function (e) {
								setExtremesHandler(e,updateLines);
							}
						}
					},
					scrollbar: {
                        enabled: false,
                        liveRedraw: false
                    },
					exporting: {
						enabled: false
					},
					yAxis: [
					{
						min: 0,
						//height: 250,
						title: {
							text: 'Water Consumption'
						},
						
						plotLines : [{
								id: 'NAverage',
								value : neigh_average,
								color : '#23AAE2',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 2
							}, 
							{
								id: 'Average',
								value : avg,
								color : 'red',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 3
							}]
						
					}],
					navigator: {
						series: {
							data: dataSerie0
						},
						adaptToUpdatedData: false,
					},
					
					series: [{
						name: 'Consumption',
						type: 'column',
						//data: [['1147651200000',49.9], ['1149120000000', 71.5], ['1151884800000', 106.4], ['1154390400000',129.2], ['1157068800000', 144.0], ['1159747200000', 176.0], ['1162339200000', 135.6], ['1164931200000', 148.5], ['1167782400000', 216.4], ['1170288000000',194.1], ['1172707200000',95.6], ['1175472000000',54.4]],
						//data: [49.9,71.5,106.4,129.2,144.0,176.0,135.6,148.5,216.4,194.1,95.6,54.4],
						data: dataSerie0,
						//dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]},
						showInLegend: false,
						color: '#23AAE2',
						zIndex: 4

					}]
					
				},function opt(charts){
					//charts.yAxis[0].plotLinesAndBands[0].label.toFront();
					//charts.yAxis[0].plotLinesAndBands[1].label.toFront();
			});
    });
		
		function setExtremesHandler(e, callback){
			//console.log(Highcharts.dateFormat(null, e.min));
			//console.log(Highcharts.dateFormat(null, e.max));
			var data = charts.series[0].data;
			var sum = 0;
			var count = 0;
			for(var i=0;i<data.length;i++){
				if(data[i].x - e.min >= 0&&e.max - data[i].x >= 0){
					sum = sum + data[i].y;
					count = count + 1;
				}
			}
			if(count!=0){
				avg = sum/count;
				charts.yAxis[0].removePlotLine('Average');
				callback(avg);

			}
			
			
		}
		
		function updateLines(avg) {
			// Callback, update lines
			charts.yAxis[0].addPlotLine({
                id: 'Average',
				value : avg,
				color : 'red',
				dashStyle : 'shortdash',
				width : 4,
				zIndex: 3
            });
		}


	
	}
		function dayRange() {
		
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]}});

		}
		function weekRange() {
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['week',[1]]]}});

		}
		function monthRange(){
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['month',[1]]] } });

		}
		

		
		function showHideLines(checkbox)
		{

			if (checkbox.checked)
			{

				if(checkbox.name=='Average'){
						charts.yAxis[0].addPlotLine({
							id: 'Average',
							value : avg,
							color : 'red',
							dashStyle : 'shortdash',
							width : 4,
							zIndex: 3
					});
				}
				else if(checkbox.name=='NAverage'){
					charts.yAxis[0].addPlotLine({
						id: 'NAverage',
						value : neigh_average,
						color : '#23AAE2',
						dashStyle : 'shortdash',
						width : 4,
						zIndex: 2
					});
				}
						
			}
			else{
				charts.yAxis[0].removePlotLine(checkbox.name);
			}
		}
		
		
		
					
							