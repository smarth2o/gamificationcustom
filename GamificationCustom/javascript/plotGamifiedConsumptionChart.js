var jsondata;
var actions_data;
var charts;
var neigh_average = 1;	
var avg;
var user = $('#user_oid').val();
var curr_url = "http://esb.smarth2o.ro:9081/consumption/ServiceViewSmartH2O/ConsumptionData/getConsumption?user_id="+user;

$.ajax({ 
 	type: 'get',
    	//url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetConsumptionData/getConsumpData.do?user_oid="+user, 
		url: "http://131.175.141.236:8080/community/RestCall.jsp?curr_url="+curr_url, 
		dataType: 'json',
    	success: function(xhr,status){   
			jsondata = xhr;
			
			$.ajax({ 
				type: 'get',
					//url: "http://localhost:8080/community/getActionsByUser.jsp", 
					//url: "http://131.175.141.236:8080/community/UserActivityCreditWebServiceREST/GetUserActions/getActions.do?user_id="+user, 
					url: "http://131.175.141.236:8080/SmartH20RestServices/RESTServices/GetActions/getActions.do?user_id="+user,
					dataType: 'json',
					success: function(xhr,status,XMLHttpRequest){   
						//actions_data = xhr.data;
						actions_data = xhr;
						onSuccess();
					},
					error: function(xhr,status){

					}
			   });
			  

				
			
    	},
    	error: function(xhr,status){

    	}
   });



   	function onSuccess(){
  
	//var json = JSON.parse(getURLParameter('var2'));
	var json = jsondata.reverse();
	var dataSerie0 = [];
	var dataSerie1 = [];

	var init_sum = 0;
	var init_count = 0;
	
	for(var i=0;i<json.length;i++){
		var obj = json[i];
		for(var key in obj){
			if(key=='timestamp'){
			   t = obj[key];
			   t = t.substring(0, 10);

			}
			else{
			   q = obj[key];
			   init_sum = init_sum + q;
			   init_count = init_count + 1;
			}
		}
		dataSerie0.push([Date.parse(t),q]);

	}
	
	
	for(var i=0;i<actions_data.length;i++){
		var obj = actions_data[i];
		for(var key in obj){
			if(key=='timestamp'){
			   t = obj[key];

			}
		}
		dataSerie1.push([Date.parse(t),1]);
	}
	
	

	avg = init_sum/init_count;

	var categoryImgs = ["GamificationCustom/images/clock.png","GamificationCustom/images/man.png","GamificationCustom/images/woman.png","GamificationCustom/images/son.png"];

         $(function() {
			charts = new Highcharts.StockChart(
				{
					chart: {
						type: 'line',
						renderTo: 'container',
						zoomType: 'x'
					},
					title: {
						text: ''
					},
					rangeSelector: {
						enabled : false
					},
					xAxis: {
						events: {
							setExtremes: function (e) {
								setExtremesHandler(e,updateLines);
							}
						}
					},
					scrollbar: {
                        enabled: false,
                        liveRedraw: false
                    },
					exporting: {
						enabled: false
					},
					tooltip: {
						shared: false,
						formatter: function() {
							var text = '';
							if(this.series.name == 'Consumption') {
								text = Highcharts.dateFormat('%A %e-%b-%Y',
                                          new Date(this.x)) +
									   '<br>' + '<span style="color:#23AAE2>\u25CF</span> ' + this.series.name + ': ' + this.y;
							} else {
								text = 'Declared Consumption';
							}
							return text;
						}
					},
					yAxis: [
					{
						min: 0,
						height: 150,
						title: {
							text: 'Water Consumption'
						},
						
						plotLines : [{
								id: 'NAverage',
								value : 1,
								color : '#23AAE2',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 2
							}, 
							{
								id: 'Average',
								value : avg,
								color : 'red',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 3
							}],
						
						
						plotBands: {
							id: 'BlockRates',
							color: '#acd5b4', // Color value
							from: '0', // Start of the plot band
							to: '0.3', // End of the plot band
							zIndex: 1
						}
						
					},
					{
						title: {
							text: 'Family Consumption'
						},
						gridLineWidth: 0,
						labels:
						{
						  style: {
										color: 'white',
									}
						},
						min : 0.5,
						max : 2.5,
						top: 150,
						bottom: 200,
						height: 100,
						offset: 0,
						plotBands: [{
							color: '#E5E5E1',
							from: '0.6',
							to: '1.4'
						  }
						  ],
						labels: {
							align: 'left',
							x: 5,
							useHTML: true,  
							formatter: function(){
								if((this.value<2)&&(this.value>0)){
								    var urlmy = $('img[id="myself"]').attr('src');
									//$('img[id="copy"]').attr("src", urlmy);
									//return '<img src="'+categoryImgs[this.value]+'"></img>';
									return '<img height="32" width="32" src="'+urlmy+'"></img>'
								}
								                        
							}
						}
					}],
					navigator: {
						series: {
							data: dataSerie0
						},
						adaptToUpdatedData: false,
					},
					
					series: [{
						name: 'Consumption',
						type: 'column',
						data: dataSerie0,
						//dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]},
						showInLegend: false,
						color: '#23AAE2',
						zIndex: 4

					},{
						name: 'Myself',
						yAxis: 1,
						data: dataSerie1,
						showInLegend: false,
						marker : {
							enabled : true,
							symbol: 'url(GamificationCustom/images/tap.png)'
						},
						lineWidth : 0,
						enableMouseTracking: true,
						states : {
							hover : false
						}
					}]
					
				},function opt(charts){
					//charts.yAxis[0].plotLinesAndBands[0].label.toFront();
					//charts.yAxis[0].plotLinesAndBands[1].label.toFront();
					//charts.yAxis[0].plotLinesAndBands[2].label.toFront();
			});
    });
		
		function setExtremesHandler(e, callback){
			//console.log(Highcharts.dateFormat(null, e.min));
			//console.log(Highcharts.dateFormat(null, e.max));
			var data = charts.series[0].data;
			var sum = 0;
			var count = 0;
			for(var i=0;i<data.length;i++){
				if(data[i].x - e.min >= 0&&e.max - data[i].x >= 0){
					sum = sum + data[i].y;
					count = count + 1;
				}
			}
			if(count!=0){
				var avg = sum/count;
				charts.yAxis[0].removePlotLine('Average');
				callback(avg);

			}
			
			
		}
		
		function updateLines(avg) {
			// Callback, update lines
			charts.yAxis[0].addPlotLine({
                id: 'Average',
				value : avg,
				color : 'red',
				dashStyle : 'shortdash',
				width : 4,
				zIndex: 3
            });
		}


	
	}
		function dayRange() {
		
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]}});
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['day',[1]]]}});

		}
		function weekRange() {
			
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['week',[1]]]}});
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['week',[1]]]}});

		}
		function monthRange(){
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['month',[1]]] } });
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['month',[1]]]}});

		}
		
		function showHideLines(checkbox)
		{

			if (checkbox.checked)
			{

				if(checkbox.name=='Average'){
						charts.yAxis[0].addPlotLine({
							id: 'Average',
							value : avg,
							color : 'red',
							dashStyle : 'shortdash',
							width : 4,
							zIndex: 3
					});
				}
				else if(checkbox.name=='NAverage'){
					charts.yAxis[0].addPlotLine({
						id: 'NAverage',
						value : neigh_average,
						color : '#23AAE2',
						dashStyle : 'shortdash',
						width : 4,
						zIndex: 2
					});
				}
						
			}
			else{

				charts.yAxis[0].removePlotLine(checkbox.name);
			}
		}
		
		function showHideBand(checkbox)
		{
			if (checkbox.checked){
				charts.yAxis[0].update({
				plotBands: {
						id: 'BlockRates',
						color: '#acd5b4', 
						from: '0', 
						to: '0.3', 
						zIndex: 1
					}
				});
			}
			else{
				charts.yAxis[0].removePlotBand('BlockRates');
			}
		}